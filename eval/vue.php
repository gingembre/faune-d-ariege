<?php
function AfficherHaut($titre='Ajouter un nouvel animal') {
    echo ("
        <!DOCTYPE html>
    <html lang='fr'>
    <head>
        <meta charset='utf-8'>
        <title>Faune d'Ariége</title>
        <link rel='shortcut icon' type='image/x-icon' href='image/icone.png'/>
        <link rel='stylesheet' href='form.css'>
        <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet'>
        <link rel='stylesheet' type='text/css' media='screen' href='form.css'>      
    </head>
    <body>
            <header>
                <div>
                    <img src='image/logo.png' alt='logo site'>
                </div>
                <div id='enTete'>
                    <h1>".$titre."<h1>
                </div>
                    <form action='index.php' method='get' id='ajouter'>
                        <button type='submit' name='choix' value='ajouter'>Ajouter</button>
                        <button type='submit' name='choix' value='accueil'>Retour à l'accueil</button>
                    </form>
            </header>
        ");
}

function AfficherFormulaire($an=null) {
    
    $id = $nom_usuel = $espece = $famille = $statut = $nbrs_representants = $description = "";
    
    if(null!==$an){
        $id = $an->getId();
        $nom_usuel = $an->getNom_usuel();
        $espece = $an->getEspece();
        $famille =$an->getFamille();
        $statut = $an->getStatut();
        $nbrs_representants = $an->getNbrs_representants();
        $description =$an->getDescription();
    }

    echo ("
                <form  id='miseEnForme' action='index.php' method='post' enctype='multipart/form-data'>
                <input type='hidden' name='id' value='".$id."'/>
                    <div>
                        <label for='name'>Nom_usuel :</label>
                        <input type='text' id='name' name='nom_usuel' value='".$nom_usuel."' required/>
                    </div>
                    <div>
                        <label for='espece'>Espece :</label>
                        <input type='text' id='espece' name='espece' value='".$espece."'required/>
                    </div>
                    <div>
                        <label for='famille'>Famille :</label>
                        <input type='text' id='famille' name='famille' value='".$famille."'required/>
                    </div>
                    <div>
                        <label for='statut'>Statut :</label>
                        <select name='statut' id='statut'>
                           <option value='Domestique'>Domestique</option>
                           <option value='Sauvage'>Sauvage</option>
                           <option value='Semi-domestique'>Semi-domestique</option>
                        </select>
                    </div>
                    <div>
                        <label for='nbrs_representants'>Nombre de représentants éstimé dans le département :</label>
                        <input type='number' id='nbrs_representants' name='nbrs_representants' value='".$nbrs_representants."' required/>
                    </div>
                    <div>
                        <label for='description'>Description de l'animal:</label></br>
                        <textarea id='description' name='description' rows='5' cols='45' placeholder='Vous pouvez écrire ici...' minlength='10' maxlength='512' required>".$description."</textarea>
                    </div>
                    <div>  
                    <label>Choisissez une photo sur votre ordinateur : </label>
                        <input id='image' type='file' name='image'/>
                    </div>
                    <div id='mil'>
                        <button type='submit' name='choix' value='enregister'>Envoyer</button>
                        <button type='submit' name='choix' value='annuler'>Annuler</button>
                    </div>
                </form>
              
        ");
}

function AfficherInfos() {

    echo '<div id="box">';

    $tab=Animal::getList();

    foreach($tab as $info){

        if(null!==$info->getId()){
            
            echo     '<p>
                      <form id="infos" action="index.php" method="get" >
                          <h2>Fiche animal n° '.$info->getId().' </h2><br />';
            
                          $benji="image/".$info->getId();
            
            if(file_exists($benji)){
                echo '<img id="redimensionImage" src="'.$benji.'" alt="image de l\'animal">';
            }
            
            echo      '   <p><b>Nom usuel : </b>'.$info->getNom_usuel().'</p>
                          <p><b>Espece : </b>'.$info->getEspece().'</p>
                          <p><b>Famille : </b>'.$info->getFamille().'</p>
                          <p><b>Statut : </b>'.$info->getStatut().'</p>
                          <p><b>Nombre de représentants éstimé dans le département : </b>'.$info->getNbrs_representants().'</p>
                          <p><b>Description: </b> '.$info->getDescription().'</p>
                            <input type="hidden" name="getinfo" value="'.$info->getId().'">
                            <button type="submit" name="choix" value="modifier">Modifier</button>
                            <button type="submit" name="choix" value="supprimer" onClick="return confirm(\'Etes-vous sur(e) de vouloir supprimer cet animal ?\');">Supprimer</button>
                      </form>';
        }
    }
    echo '  </p>
          </div>';
}

function AfficherBas() {
    echo ("
            <footer>
                <p>By Damien Eychenne</p>
            </footer>
        </body>
        </html>
        ");
}

?>