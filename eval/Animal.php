<?php 
class Animal {
    protected $id;
    protected $nom_usuel;
    protected $espece;
    protected $famille; 
    protected $statut; 
    protected $nbrs_ariege;
    protected $description; 

    function __construct($id, $nu=null, $e=null, $f=null, $s=null, $na=null, $d=null) {
        if($id == null){
            $this->id                 = $id;
            $this->nom_usuel          = $nu;
            $this->espece             = $e;
            $this->famille            = $f;
            $this->statut             = $s;
            $this->nbrs_representants = $na;
            $this->description        = $d;
        }
        else {
            $this->load($id);
        }
    }

    public function getId() { return $this->id;}
    public function getNom_usuel() { return $this->nom_usuel;}
    public function getEspece() { return $this->espece;}
    public function getFamille() { return $this->famille;}
    public function getStatut() { return $this->statut;}
    public function getNbrs_representants() { return $this->nbrs_representants;}
    public function getDescription() { return $this->description;}

    public function setNom_usuel($nu) { $this->nom_usuel=$nu; }
    public function setEspece($e) { $this->espece=$e; }
    public function setFamille($f) { $this->famille=$f; }
    public function setStatut($s) { $this->statut=$s; }
    public function setNbrs_representants($na) { $this->nbrs_representants=$na; }
    public function setDescription($d) { $this->description=$d; }

    public function save() {
        require('modele.php');
        $requete = 'INSERT INTO Animal(nom_usuel,  espece, famille, statut, nbrs_representants, description) values (?,?,?,?,?,?)';
        $req = $bdd->prepare($requete);
        $req->bindParam(1, $this->nom_usuel);
        $req->bindParam(2, $this->espece);
        $req->bindParam(3, $this->famille);
        $req->bindParam(4, $this->statut);
        $req->bindParam(5, $this->nbrs_representants);
        $req->bindParam(6, $this->description);
        if($req->execute()==false){
			die('erreur :'.$requete);
          
        }
        else {
            $this->id=$bdd->lastInsertId();
        }
	
    }

    protected function load($id) {
        require('modele.php');
        $req1 = $bdd->prepare('SELECT * FROM Animal WHERE ID = ?');
        $req1->bindParam(1, $id);
        $req1->execute();
        $fetch = $req1->fetch(PDO::FETCH_ASSOC);

        $this->id = $fetch['ID'];
        $this->nom_usuel = $fetch['nom_usuel'];
        $this->espece = $fetch['espece'];
        $this->famille = $fetch['famille'];
        $this->statut = $fetch['statut'];
        $this->nbrs_representants = $fetch['nbrs_representants'];
        $this->description = $fetch['description'];
        
    }

    static function getList(){
        require('modele.php');    
        $tabAnimal = array();

        $req2 = $bdd->prepare('SELECT ID FROM Animal');
        $req2->execute();
        

        while($inf = $req2->fetch(PDO::FETCH_ASSOC)){
            array_push($tabAnimal, new Animal($inf['ID']));
        }

        return $tabAnimal;
    }

    public function update(){
        require('modele.php');

        $req3 = $bdd->prepare('UPDATE Animal SET nom_usuel = ?, espece = ?, famille = ?, statut = ?, nbrs_representants = ?, description = ? WHERE ID = ?');
        $req3->bindParam(1, $this->nom_usuel);
        $req3->bindParam(2, $this->espece);
        $req3->bindParam(3, $this->famille);
        $req3->bindParam(4, $this->statut);
        $req3->bindParam(5, $this->nbrs_representants);
        $req3->bindParam(6, $this->description);
        $req3->bindParam(7, $this->id);
        $req3->execute();

    }
    
    public function delete(){
		require('modele.php');
		$requete = 'DELETE FROM `Animal` WHERE ID = ?';
		$req = $bdd->prepare($requete);
		$req->bindParam(1, $this->id);
		if($req->execute()==false){
			die('erreur :'.$requete);
		}
	} 
}
?>