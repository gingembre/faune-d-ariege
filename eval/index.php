<?php
require('Animal.php');
require('vue.php');
require('upload.php');


$choix = 'accueil';

if(isset($_REQUEST['choix'])){
    $choix = $_REQUEST['choix'];
}

switch($choix) {
        
    case 'accueil' :    AfficherHaut("Bienvenue");
                        AfficherInfos();
                
                        break;
        
    case 'supprimer' :  AfficherHaut("La fiche n°".$_REQUEST['getinfo']." à bien était supprimé.");
                        if(isset($_REQUEST['getinfo']) and $_REQUEST['getinfo'] != ""){
                            $suppr = new Animal($_REQUEST['getinfo']);
                            $suppr->delete();
				        }
                        AfficherInfos();
                        break;
        
    case 'ajouter' :    AfficherHaut("Merci d'ajouter un nouvel animal :) ");
                        AfficherFormulaire();

                        break;

    case 'enregister' : AfficherHaut("<h1>Vous avez enregistré ou modifié un nouvel animal</h1><h2>(".$_REQUEST['nom_usuel'].")</h2>");
                               
                            if($_REQUEST['id']!=""){ //modif
                                $ani = new Animal($_REQUEST['id']);
                                $ani->setNom_usuel($_REQUEST['nom_usuel']);
                                $ani->setEspece($_REQUEST['espece']);
                                $ani->setFamille($_REQUEST['famille']);
                                $ani->setStatut($_REQUEST['statut']);
                                $ani->setNbrs_representants($_REQUEST['nbrs_representants']);
                                $ani->setDescription($_REQUEST['description']);
                                $ani->update();
                                if(isset($_FILES)){
                                upload($_REQUEST['id']);
                                }
                            }
                            else{                    //nouveau
                                $ani = new Animal(null, $_REQUEST['nom_usuel'], $_REQUEST['espece'], $_REQUEST['famille'], $_REQUEST['statut'], $_REQUEST['nbrs_representants'], $_REQUEST['description']);
                                $ani->save();
                                if(isset($_FILES)){
                                upload($ani->getId());
                                }
                            }
                            
                        AfficherInfos();

                        break;

    case 'annuler' :    AfficherHaut("De retour à nos differentes fiches ! ");
                        AfficherInfos();
                        break;

    case 'modifier' :   AfficherHaut("Vous modifiez la fiche n°".$_REQUEST['getinfo']."");
                        $an = new Animal($_REQUEST['getinfo']);
                        AfficherFormulaire($an);
                        AfficherInfos();
                        break; 
        
    default :           break;
}

AfficherBas();
?>