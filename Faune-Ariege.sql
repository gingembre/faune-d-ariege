-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Client :  localhost:3306
-- Généré le :  Ven 17 Mai 2019 à 12:10
-- Version du serveur :  5.7.26-0ubuntu0.18.04.1
-- Version de PHP :  7.2.17-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `Faune-Ariege`
--

-- --------------------------------------------------------

--
-- Structure de la table `Animal`
--

CREATE TABLE `Animal` (
  `ID` int(11) NOT NULL,
  `nom_usuel` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `famille` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `statut` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `nbrs_representants` int(11) NOT NULL,
  `description` varchar(513) COLLATE utf8_unicode_ci NOT NULL,
  `espece` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `Animal`
--

INSERT INTO `Animal` (`ID`, `nom_usuel`, `famille`, `statut`, `nbrs_representants`, `description`, `espece`) VALUES
(1, 'Souris blanche', 'Rongeur', 'Domestique', 3000000, 'Petit rongeur nocturne, de couleur blanche ou grisâtre, souvent rencontré dans les habitations. Exemple : La souris blanche est souvent adoptée comme animal de compagnie et sert aussi de cobaye pour la recherche scientifique.', 'Souris'),
(5, 'Border Collie', 'Canidé', 'Domestique', 123, 'Le border collie est une race de chien de troupeau originaire de la frontière entre l\'Angleterre et l\'Écosse. Ce chien de travail seconde les agriculteurs dans la conduite du bétail dans le monde entier, notamment les moutons. Il est connu pour son intelligence et son obéissance.', 'Chien'),
(6, 'Chihuahua', 'Canidé', 'Domestique', 2000, 'Le chihuahua est le chien le plus petit du monde. Son nom vient de l’état mexicain homonyme. Il pèse quelquefois moins d’un kilogramme mais la fourchette idéale est comprise entre 1,5 et 3 kg.', 'Chien'),
(37, 'Escargot Ariégeois', 'Molusque', 'Domestique', 26000000, 'Le terme escargot est un nom vernaculaire qui en français désigne des gastéropodes à coquille, généralement terrestres et appelés aussi des colimaçons, par opposition aux limaces.', 'Molusque'),
(49, 'Lérot', 'Rongeur', 'Domestique', 20000, 'Vous avez peut-être aperçu le soir, dans les arbres de votre jardin ou en forêt, ce petit animal qui ressemble à un hamster à lunettes. Il mesure environ 15 cm + 10 cm de queue, il a de grandes oreilles arrondies, son poil est gris sur le dessus avec une bande noire sous les oreilles et un cercle noir autour des yeux. Une tache plus sombre également  aux  dessus des  pattes antérieures.  Sa gorge et  son ventre sont  blancs.  Sa queue est velue avec la pointe en pinceau blanc bordé de noir.', 'Eliomys quercinus');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `Animal`
--
ALTER TABLE `Animal`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `Animal`
--
ALTER TABLE `Animal`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
